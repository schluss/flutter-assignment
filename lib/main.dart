import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Assignment',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Assignment'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String _primaryAccountNumer = '';

  void _runAssignment(){

    /// 1. Retrieve the Sample Bankstatement PDF file
    
    /// 2. Extract the value displayed next to Primary Account Number
     
    /// 3. Store the extracted Primary Account Number securely
    
    /// 4. Get the stored Primary Account Number
    
    /// 5. Display the Primary Account Number
    setState(() {
      _primaryAccountNumer = '...';
    });

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),
      body: Center(

        child: Column(

          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Primary Account Number:',
            ),
            Text(
              _primaryAccountNumer,
              style: Theme.of(context).textTheme.headline4,
            ),

            Container(
              height: 20,
            ),

            FloatingActionButton(
        onPressed: _runAssignment,
        tooltip: 'Retrieve Primary Account Number',
        child: const Icon(Icons.download),
      ),
          ],
        ),
      ),
      
    );
  }
}
